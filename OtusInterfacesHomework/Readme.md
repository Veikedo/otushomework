# Домашнее задание по теме "Интерфейсы и их особенности"

1. Реализован класс OtusStreamReader. Работа с объектом класса продемонстрирована в методе Main.
2. В классе OtusStreamReader реализован интерфейс IAlgorithm<T>, выполняющий сортировку по заданному ключу.
3. Добавлен класс AccountRepository, сохраняющий значения в JSON-файл (библиотека Newtonsoft.Json). Добавлен класс AccountService, реализующий IAccountService. Для класса AccountService реализованы тесты с помощью Moq. 