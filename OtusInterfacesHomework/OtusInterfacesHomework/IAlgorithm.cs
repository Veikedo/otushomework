﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusInterfacesHomework
{
    internal interface IAlgorithm<T>
    {
        IEnumerable<T> Sort<TKey>(Func<T, TKey> keySelector);
    }
}
