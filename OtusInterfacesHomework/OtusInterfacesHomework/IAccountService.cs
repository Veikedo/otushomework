﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("AccountRepositoryTests")]
//to Moq
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace OtusInterfacesHomework
{
    internal interface IAccountService
    {
        void AddAccount(Account account);
    }
}
