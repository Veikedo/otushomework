﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace OtusInterfacesHomework
{
    internal interface ISerializer
    {
        string Serialize<T>(T item);
        T Deserialize<T>(Stream stream);
    }
}
