﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusInterfacesHomework
{
    internal class AccountService : IAccountService
    {
        public AccountService(IRepository<Account> repository)
        {
            accountRepo = repository;
        }
        public void AddAccount(Account account)
        {
            if (String.IsNullOrEmpty(account.FirstName)) throw new ArgumentException("FirstName is null or empty!");
            if (String.IsNullOrEmpty(account.LastName)) throw new ArgumentException("LastName is null or empty!");
            if ((DateTime.Now.Year - account.BirthDate.Year) < 18) throw new ArgumentException("age less than 18!");

            accountRepo.Add(account);
        }

        private readonly IRepository<Account> accountRepo;
    }
}
