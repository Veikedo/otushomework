﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System.Xml;
using System.Xml.Serialization;

namespace OtusInterfacesHomework
{
    internal class OtusXmlSerializer : ISerializer
    {

        public T Deserialize<T>(Stream stream)
        {
            var serializer = new ConfigurationContainer().Type<T>().Create();
            return serializer.Deserialize<T>(new XmlReaderSettings { IgnoreWhitespace = false }, stream);
        }

        public string Serialize<T>(T item)
        {
            var serializer = new ConfigurationContainer().Create();
            return serializer.Serialize(new XmlWriterSettings { Indent = true }, item);
        }
    }
}
