﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OtusInterfacesHomework
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create Array of Person for test
            var persons = new Person[]
            {
                new Person("John", "Wick", 42),
                new Person("Adam", "Sendler", 41),
                new Person("Ivan", "Ivanov", 25),
            };

            var xmlSerializer = new OtusXmlSerializer();

            string serialized = xmlSerializer.Serialize(persons);

            UTF8Encoding utf8Encoding = new UTF8Encoding();
            
            //Serialize array of Person into XML file
            using FileStream stream = new FileStream("test.xml", FileMode.Create);
            using var sw = new StreamWriter(stream, utf8Encoding);
            sw.Write(serialized);
            sw.Flush();
            stream.Seek(0, SeekOrigin.Begin);



            var streamReader = new OtusStreamReader<Person>(xmlSerializer, stream);
            var personSorter = new PersonSorter();


            Console.WriteLine("Printing a list of Person sorted using PersonSorter:");
            foreach (var p in personSorter.Sort(streamReader))
            {
                Console.WriteLine($" -{p}");
            }
            Console.WriteLine();
            Console.WriteLine("Printing a list of Person sorted by age:");
            stream.Seek(0, SeekOrigin.Begin);
            foreach (var p in streamReader.Sort(x => x.Age))
            {
                Console.WriteLine($" -{p}");
            }
            Console.WriteLine();

            //Create AccountRepository
            var jsonSerializer = new OtusJsonSerializer();
            var accountRepo = new AccountRepository(jsonSerializer, "AccountRepo.json");
            var accountService = new AccountService(accountRepo);

            //fill AccountRepository with AccountService
            if (accountRepo.IsRepositoryEmpty())
            {
                var ivanAccount = new Account() { BirthDate = DateTime.Parse("06.05.1991"), FirstName = "Ivan", LastName = "Ivanov" };
                var petrAccount = new Account() { BirthDate = DateTime.Parse("04.05.1971"), FirstName = "Petr", LastName = "Petrov" };
                accountService.AddAccount(ivanAccount);
                accountService.AddAccount(petrAccount);
            }


            var invalidAccount = new Account() { BirthDate = DateTime.Now, FirstName = null, LastName = "Ivanov" };

            try
            {
                accountService.AddAccount(invalidAccount);
            }
            catch(ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine();


            Console.WriteLine("Accounts in repository:");
            foreach(var account in accountRepo.GetAll())
            {
                Console.WriteLine($" -{account}");
            }


            Console.WriteLine("Account with FirstName == Ivan:");
            Console.WriteLine($" -{accountRepo.GetOne(x => x.FirstName == "Ivan")}");

        }
    }
}
