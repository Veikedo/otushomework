﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;

namespace OtusInterfacesHomework
{
    internal class AccountRepository : IRepository<Account>
    {

        public AccountRepository(ISerializer customSerializer, string repositoryName)
        {
            serializer = customSerializer;
            RepositoryName = repositoryName;
            accounts = GetAccounts();
        }
        public void Add(Account item)
        {
            
            accounts.Add(item);
            string repositorydata = serializer.Serialize(accounts);
            UTF8Encoding utf8Encoding = new UTF8Encoding();

            using FileStream stream = new FileStream(RepositoryName, FileMode.OpenOrCreate);
            using var sw = new StreamWriter(stream, utf8Encoding);
            sw.Write(repositorydata);
            sw.Flush();
        }

        public IEnumerable<Account> GetAll()
        {
            foreach (var account in accounts)
            {
                yield return account;
            }
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            return this.GetAll().FirstOrDefault(predicate);
        }

        private List<Account> GetAccounts()
        {
            try
            {
                using FileStream stream = new FileStream(RepositoryName, FileMode.Open);
                return serializer.Deserialize<List<Account>>(stream);
            } 
            catch(FileNotFoundException)
            {
                return new List<Account>();
            }

        }

        public bool IsRepositoryEmpty()
        {
            return accounts.Count == 0;
        }

        public string RepositoryName { get; }
        private readonly ISerializer serializer;
        private readonly List<Account> accounts;
    }
}
