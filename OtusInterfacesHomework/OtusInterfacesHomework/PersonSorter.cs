﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace OtusInterfacesHomework
{
    internal class PersonSorter : ISorter<Person>
    {
        public IEnumerable<Person> Sort(IEnumerable<Person> notSortedItems)
        {
            return notSortedItems.OrderBy(x => x.FirstName);
        }
    }
}
