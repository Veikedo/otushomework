﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusInterfacesHomework
{
    internal interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        T GetOne(Func<T, bool> predicate);
        void Add(T item);
    }
}
