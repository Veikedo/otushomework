﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using System.Linq;

namespace OtusInterfacesHomework
{
    internal class OtusStreamReader<T> : IEnumerable<T>, IAlgorithm<T>
    {


        private readonly Stream stream;
        private readonly ISerializer serializer;

        public OtusStreamReader(ISerializer customSerializer, Stream inputStream) 
        {
            serializer = customSerializer;
            stream = inputStream;
        }


        public IEnumerator<T> GetEnumerator()
        {
            var items = serializer.Deserialize<T[]>(stream);

            foreach(T item in items)
            {
                yield return item;
            }

        }

        public IEnumerable<T> Sort<TKey>(Func<T, TKey> keySelector)
        {
            return this.OrderBy(keySelector);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            var items = serializer.Deserialize<T[]>(stream);

            foreach (T item in items)
            {
                yield return item;
            }
        }
    }
}
