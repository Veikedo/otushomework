﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusInterfacesHomework
{
    internal interface ISorter<T>
    {
        IEnumerable<T> Sort(IEnumerable<T> notSortedItems);
    }
}
