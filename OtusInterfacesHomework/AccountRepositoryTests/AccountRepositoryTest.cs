using System.Collections.Generic;
using System;
using Moq;
using Xunit;

namespace OtusInterfacesHomework.Tests
{
    public class AccountRepositoryTest
    {
        [Fact]
        public void AddAccountTest()
        {
            var mock = new Mock<IRepository<Account>>();

            mock.Setup(foo => foo.Add(It.IsAny<Account>()));

            IAccountService service = new AccountService(mock.Object);
            
            service.AddAccount(new Account() 
            { 
                BirthDate = DateTime.Parse("01.01.1983"), 
                LastName = "Petrov", 
                FirstName = "Petr"
            } 
            );

            mock.Verify(foo => foo.Add(It.IsAny<Account>()));
        }

        [Fact]
        public void AddInvalidAccountTest()
        {
            var mock = new Mock<IRepository<Account>>();

            mock.Setup(foo => foo.Add(It.IsAny<Account>()))
                .Throws(new ArgumentException());

            IAccountService service = new AccountService(mock.Object);

            Assert.Throws<ArgumentException>(() =>
                service.AddAccount(
                    new Account() 
                    { 
                        BirthDate = DateTime.Now, 
                        LastName = "Petrov", 
                        FirstName = "Petr" 
                    }
                    )
                );
        }
    }
}
