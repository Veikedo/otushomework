﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusOperatorsHomework
{
    /// <summary>
    /// Структура описывает цвет в цветовой модели RGB
    /// </summary>
    internal struct Color
    {

        public Color(byte red = 0, byte green = 0, byte blue = 0)
        {
            Red = red;
            Green = green;
            Blue = blue;
        }


        public static Color operator +(Color first, Color second)
        {
            //mix 2 colors
            return new Color(
                (byte)Math.Min(first.Red + second.Red, 255),
                (byte)Math.Min(first.Green + second.Green, 255),
                (byte)Math.Min(first.Blue + second.Blue, 255)
                );
        }

        public static bool operator ==(Color first, Color second) 
        {
            return (
                first.Red == second.Red && 
                first.Green == second.Green && 
                first.Blue == second.Blue
                );
        }

        public static bool operator !=(Color first, Color second)
        {
            return !(first == second);
        }


        public override string ToString()
        {
            return $"Red: 0x{Red:X}; Green: 0x{Green:X}; Blue: 0x{Blue:X}";
        }

        public byte Red { get; set; }
        public byte Green { get; set; }
        public byte Blue { get; set; }

    }
}
