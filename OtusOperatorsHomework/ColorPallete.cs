﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusOperatorsHomework
{
    internal class ColorPallete
    {
        public ColorPallete()
        {
            pallete = new List<Color>();
        }

        public Color this[int index]
        {
            get
            {   
                return pallete[index];
            }
            set
            {
                pallete[index] = value;
            }
        }

        public int Length 
        {
            get
            {
                return pallete.Count;
            }
        }

        public void AddColor(Color color)
        {
            pallete.Add(color);
        }

        public void RemoveColor(Color color)
        {

            foreach(var colorToDelete in pallete)
            {
                if (colorToDelete == color)
                {
                    pallete.Remove(colorToDelete);
                    return;
                }
            }

            throw new ArgumentException();
        }

        private readonly List<Color> pallete;
    }
}
