﻿using System;

namespace OtusOperatorsHomework
{
    class Program
    {

        private static void PrintPallete(ColorPallete pallete)
        {
            Console.WriteLine("Colors in pallete:");
            for (int i = 0; i < pallete.Length; i++)
            {
                Console.WriteLine($" - {pallete[i].GetColorName()}");
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            //create and fill color pallete
            var pallete = new ColorPallete();
            pallete.AddColor(new Color(255, 0, 0));
            pallete.AddColor(new Color(128, 128, 128));
            pallete.AddColor(new Color(0, 0, 255));


            PrintPallete(pallete);

            Color lime = new Color(0, 255);
            int colorIndex = 0;
            Console.WriteLine($"Mixing {pallete[colorIndex].GetColorName()} from pallete and {lime.GetColorName()}");

            pallete[colorIndex] = pallete[colorIndex] + lime;

            PrintPallete(pallete);


            colorIndex = 2;
            Console.WriteLine($"Remove {pallete[colorIndex].GetColorName()} color  from pallete");
            pallete.RemoveColor(pallete[colorIndex]);

            PrintPallete(pallete);

            Console.ReadKey();
        }
    }
}
