﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusOperatorsHomework
{
    internal static class ColorExtension
    {

        static ColorExtension()
        {
            colorTable = new Dictionary<Color, string>
            {
                { new Color(255, 255, 255), "White" },
                { new Color(0, 0, 0), "Black" },
                { new Color(255, 0, 0), "Red" },
                { new Color(0, 255, 0), "Lime" },
                { new Color(0, 0, 255), "Blue" },
                { new Color(255, 255, 0), "Yellow" },
                { new Color(0, 255, 255), "Cyan" },
                { new Color(255, 0, 255), "Magenta" },
                { new Color(192, 192, 192), "Silver" },
                { new Color(128, 128, 128), "Gray" },
                { new Color(128, 0, 0), "Maroon" },
                { new Color(128, 128, 0), "Olive" },
                { new Color(0, 128, 0), "Green" },
                { new Color(128, 0, 128), "Purple" },
                { new Color(0, 128, 128), "Teal" },
                { new Color(0, 0, 128), "Navy" },
            };

        }
        /// <summary>
        /// Возвращает строковое представление одного из основных цветов
        /// </summary>
        public static string GetColorName(this Color color)
        {

            if (colorTable.ContainsKey(color)) {
                return colorTable[color];
            }

            //just return RGB components
            return color.ToString();
        }

        private static readonly Dictionary<Color, string> colorTable;
    }
}
